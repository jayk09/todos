import { writable } from "svelte/store";
import TodosService from "../services/todos.service";

const todos = writable([]);
let uid = 0;
export default {
	subscribe: todos.subscribe,
	init: async () => {
		const items = await TodosService.get();
		todos.set([...items]);
		uid = items.length;
	},
	add: input => {
		const item = {
			id: ++uid,
			title: input,
			done: false
		};
		todos.update(items => [item, ...items]);
	},
	remove: ({ detail }) =>
		todos.update(items => items.filter(i => i !== detail)),
	update: ({ detail }) =>
		todos.update(items => [detail, ...items.filter(i => i !== detail)])
};
